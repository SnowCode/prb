package labo2;
import io.Console;

public class VenteVoiture {
    public static void main(String[] args) {
        // Variable de départ
        double montantHtva;

        // Variable de transition
        double montantTvac;

        // Variables de résultat
        double montantTva, montantRemise, montantTotal;

        // Variable connues
        final double tva = 0.21;
        final double remise = 0.03;

        // Demande du montant hors TVA
        System.out.print("Montant HTVA de la voiture : ");
        montantHtva = Console.lireDouble();

        // Calcul du TVAC et du montant avec TVA
        montantTva = montantHtva * tva;
        montantTvac = montantHtva + montantTva;

        // Calcul du montant remise et du montant final
        montantRemise = montantTvac * remise;
        montantTotal = montantTvac - montantRemise;

        // Affichage des résultats
        System.out.printf("Montant de la TVA : %f\n", montantTva);
        System.out.printf("Montant de la remise : %f\n", montantRemise);
        System.out.printf("Montant final : %f\n", montantTotal);
    }
}
