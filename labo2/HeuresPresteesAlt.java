package labo2;
import io.Console;

public class HeuresPresteesAlt {
    public static void main (String[] args) {
        double tarifHoraire = 18.75;
        double tarifMinutes = tarifHoraire / 60;

        String[] jours = { "lundi", "mardi", "mercredi", "jeudi", "vendredi" };
        int minutesTotales = 0;
        for (String jour: jours) {
            System.out.printf("Prestations du %s ? ", jour);
            String input = Console.lireString();
            int heures = Integer.parseInt(input.substring(0, input.length() - 3));
            int minutes = Integer.parseInt(input.substring(input.length() - 2, input.length()));
            minutesTotales += heures * 60 + minutes;
        }

        int heures = minutesTotales / 60;
        int minutes = minutesTotales % 60;
        System.out.printf("Durée hebdomadaire : %d:%d\n", heures, minutes);

        double montantTotal = minutesTotales * tarifMinutes;
        System.out.printf("Montant à facturer : %.2f EUR\n", montantTotal);
    }
}
