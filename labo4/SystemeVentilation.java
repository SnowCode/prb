package labo4;
import io.Console;

public class SystemeVentilation {
    public static void main(String[] args) {
        // Variables
        char typeChauffage;
        char saturation;
        int humidite;

        boolean ventilateurExtraction = false;
        boolean ventilateurAlimentation = false;

        // Input
        System.out.print("Quel est le type de chauffage : (A)ir chaud puisé, (C)ombustion ? ");
        typeChauffage = Console.lireChar();

        System.out.print("Le sol est-il saturé de gaz : (O)ui, (N)on ? ");
        saturation = Console.lireChar();

        System.out.print("Quel est le taux d'humidité en % ? ");
        humidite = Console.lireInt();

        // Traitement
        // Rendre le tout case insensitive
        typeChauffage = Character.toLowerCase(typeChauffage);
        saturation = Character.toLowerCase(saturation);
        
        // Traiter chaque cas, si aucune valeur n'est changée et que alimentation et extractions sont tous les deux à false alors l'input était invalide
        if (typeChauffage == 'a') {
            if (saturation == 'o') {
                ventilateurAlimentation = true;
                ventilateurExtraction = true;
            } else if (saturation == 'n') {
                ventilateurExtraction = true;
            }
        } else if (typeChauffage == 'c') {
            if (humidite > 70) {
                ventilateurAlimentation = true;
                ventilateurExtraction = true;
            } else if (humidite <= 70) {
                ventilateurAlimentation = true;
            }
        }

        // Affichage des résultats
        if (ventilateurAlimentation == false && ventilateurExtraction == false) {
            System.out.println("Réponses incorrectes !");
        } else if (ventilateurAlimentation == true && ventilateurExtraction == false) {
            System.out.println("Installer des ventilateurs d'alimentation");
        } else if (ventilateurAlimentation == false && ventilateurExtraction == true) {
            System.out.println("Installer des ventilateurs d'extraction");
        } else if (ventilateurAlimentation == true && ventilateurExtraction == true) {
            System.out.println("Installer des ventilateurs d'alimentation et d'extraction");
        }
    }
}
