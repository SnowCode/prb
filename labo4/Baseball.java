package labo4;
import io.Console;

public class Baseball {
    public static void main(String[] args) {
        // Variables
        char temps;
        int humidite;
        boolean valide = true; // La variable vaut 1 si les réponses sont cohérentes
        boolean status = true; // La variable vaut 1 si le match a lieu

        // Input
        System.out.print("Quel temps fait-il : (N)uageux, (E)nsoleillé, (P)luvieux ? ");
        temps = Console.lireChar();
        System.out.print("Quel est le taux d'humidité en % ? ");
        humidite = Console.lireInt();

        // Traitement
        temps = Character.toLowerCase(temps); // Rendre le cas case-insensitive
        if (humidite > 100 || humidite < 0) {
            valide = false;
        } else {
            if (temps == 'n') {
                status = true;
            } else if (temps == 'p') {
                status = false;
            } else if (temps == 'e') {
                if (humidite < 90) {
                    status = true;
                } else {
                    status = false;
                }
            } else {
                valide = false;
            }
        }

        // Affichage des résultats
        if (valide == true) {
            if (status == true) {
                System.out.println("Le match sera joué");
            } else {
                System.out.println("Le match sera reporté");
            }
        } else {
            System.out.println("Réponses incorrectes !");
        }
    }
}
