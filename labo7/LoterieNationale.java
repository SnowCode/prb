package labo7;

public class LoterieNationale {

	public static void main(String[] args) {
		int option = 0;
		boolean grilleRemplie = false;
		int[] grille = new int[6];
		while (option != 3) {
			// Afficher le menu
			System.out.println("Loterie Nationale");
			if (grilleRemplie == false) {
				System.out.println("1. Remplir une grille de Lotto");
			} else {
				System.out.println("1. Modifier ma grille de Lotto");
			}
			System.out.println("2. Consulter mon gain pour le dernier tirage");
			System.out.println("3. Quitter");
			
			// Prendre le choix de l'utilisateur
			System.out.print("Choix ? ");
			option = io.Console.lireInt();
			System.out.println(""); // Affichage d'une ligne vide pour une meilleure vue
			
			if (option == 1) {
				// Encoder une grille et afficher la grille
				grille = Lotto.encoderGrille(6, 45);
				System.out.printf("Votre grille est %s\n\n", util.TableauEntiers.toString(grille));
				
				// Définir la grille comme remplie
				grilleRemplie = true;
			} else if (option == 2) {
			    int[] tirage = Lotto.genererTirage(7, 45);
				
				System.out.printf("\nLe tirage est %s\n", util.TableauEntiers.toString(tirage));
				if (grilleRemplie == false) {
					System.out.println("Vous n'avez pas rempli de grille\n");
				} else {
					// Afficher les numéros gagnants
					int[] numerosGagnant = Lotto.compterNumerosGagnants(tirage, grille);
					System.out.printf("Vous avez %d numéros gagnant", numerosGagnant[0]);
					if (numerosGagnant[1] == 1) {
						System.out.print(" et le numéro bonus\n");
					} else {
						System.out.print("\n");
					}
					
					// Calculer et afficher le gain
					int rang = Lotto.determinerRang(numerosGagnant);
					double gain = Lotto.obtenirGain(rang);
					System.out.printf("Vous gagnez %.2f euros\n\n", gain);
					
				}
			} else if (option == 3) {
				System.out.println("\nFin du programme");
			} else {
				System.out.println("Choix inccorrect !\n");
			}
		}
	}

}
