package labo7;

public class Blackjack {
	public static void main(String[] args) {
		// 1. Variables (main des joueurs + jeu de carte)
		String[] mainJoueur = new String[15];
		String[] mainCroupier = new String[15];
		int pointsJoueur = 0;
		int pointsCroupier = 0;
		String[] jeuDeCartes = getJeuDeCartes();
		
		// Compteur de cartes pour éviter de tirer deux fois la même carte
		int compteur = 0;
		
		// Compteur de cartes du joueur et du croupier pour s'arrêter avant les "NULL"
		int compteurJoueur = 0;
		int compteurCroupier = 0;
		
		// La variable qui indique si le joueur continue de tirer
		boolean tirage = true;
				
		// Melanger le jeu de cartes
		util.TableauChaines.melanger(jeuDeCartes);
		
		
		// Donner 2 cartes au joueur et deux cartes au croupier
		for (int i = 0; i < 2; i++) {
			mainJoueur[compteurJoueur] = jeuDeCartes[compteur];
			compteur++;
			compteurJoueur++;
			mainCroupier[compteurCroupier] = jeuDeCartes[compteur];
			compteur++;
			compteurCroupier++;
		}
		
	
		do {
			// Calculer les points 
			pointsJoueur = getValeurCartes(mainJoueur, 42);
			pointsCroupier = getValeurCartes(mainCroupier, 42);
			
			// Afficher les scores
			if (tirage == true) {
				afficherScore("Vous avez %d points :\n", mainJoueur, pointsJoueur);
			}
			afficherScore("Le croupier a %d points :\n", mainCroupier, pointsCroupier);
			
			// Vérifier les scores avant de continuer
			if (pointsJoueur == 21 && pointsCroupier == 21 || (tirage == false && pointsJoueur == pointsCroupier)) {
				System.out.println("C'est une égalité !");
				break;
			} else if (pointsJoueur == 21 || pointsCroupier > 21 || (tirage == false && pointsJoueur > pointsCroupier)) {
				System.out.println("Vous gagnez !");
				break;
			} else if (pointsCroupier == 21 || pointsJoueur > 21 || (tirage == false && pointsCroupier > pointsJoueur)) {
				System.out.println("Vous perdez !");
				break;
			}

			// Demander au joueur pour tirer une carte
			if (tirage == true) {
				System.out.print("Tirer une carte : (O)ui/(N)on ? ");
				if (io.Console.lireString().equals("o")) {
					mainJoueur[compteurJoueur] = jeuDeCartes[compteur];
					compteur++;
					compteurJoueur++;
				} else {
					tirage = false;
				}
			}
			
			// Si le croupier a moins de 17 points, il tire une carte
			if (pointsCroupier < 17) {
				mainCroupier[compteurCroupier] = jeuDeCartes[compteur];
				compteur++;
				compteurCroupier++;
			}
		} while (true);
	}

	private static String[] getJeuDeCartes() {
		// Constantes
		final String[] COULEURS = { "coeur", "carreau", "trèfle", "pique" };
		final String[] CARTES = { "2", "3", "4", "5", "6", "7", "8", "9",
		"10", "Valet", "Dame", "Roi", "As" };
		
		// Variable pour le résultat
		String[] jeuDeCartes;
		
		// Créer un jeu de cartes
		jeuDeCartes = new String[COULEURS.length * CARTES.length];
		for (int i = 0; i < COULEURS.length; i++) {
			for (int j = 0; j < CARTES.length; j++) {
				jeuDeCartes[CARTES.length * i + j] = CARTES[j] + " de " + COULEURS[i];
			}
		}
		
		// Retourner le jeu de cartes
		return jeuDeCartes;
	}
	
	public static int getValeurCarte(String carte) {
	    int valeur = 0;
	    String nombre = carte.split(" ")[0];
	    if (nombre.matches("^[2-9]$")) {
	      valeur = Integer.parseInt(nombre);
	    } else if (nombre.matches("^As$")) {
	      valeur = 11;
	    } else {
	      valeur = 10;
	    }

	    return valeur;
	}
	
	public static int getValeurCartes(String[] cartes, int nbCartes) {
		int total = 0;
		for (String carte: cartes) {
			if (carte == null) {
				break;
			} else {
				total += getValeurCarte(carte);
			}
		}
		
		return total;
	}
	
	public static void afficherScore(String message, String[] main, int points) {
		String affichage = "";
		System.out.printf(message, points);
		for (String carte: main) {
			if (carte == null) {
				break;
			} else {
				affichage += carte + ", ";
			}
		}
		affichage = affichage.replaceAll(", $", "\n");
		System.out.println(affichage);
	}
}