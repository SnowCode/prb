package labo7;

import java.util.Arrays;

public class Lotto {
	static int[] genererTirage(int nbTirages, int numeroMax) {
		// Générer le tableau de base
		int[] numeros = new int[numeroMax];
		
		// Remplir le tableau des numéros allant de 1 à numeroMax (j représente la position dans le tableau)
		int j = 0;
		while (j <= numeroMax - 1) {
			numeros[j] = j + 1;
			j++;
		}
		
		// Création du tableau de résultat
		int[] resultat = new int[nbTirages];
		
		// Remplir le tableau avec les différents tirages. (i représente les indices du tableau)
		int i = 0;
		while(nbTirages > 0) {
			// Choisir un nombre aléatoire pour la position (j est actuellement égal à la dernière position du tableau)
			int p = io.Aleatoire.aleatoire(0, j - 1);
			
			// Copier le numéro à la position p dans le nouveau tableau
			resultat[i] = numeros[p];
			i++;
			
			// Permuter les numéros aux positions p et j
			util.TableauEntiers.permuter(numeros, p, j-1);
			
			// Diminuer j de 1 pour ne plus avoir ce nombre
			j--;
			
			// Diminuer de 1 le nombre de tirages restant
			nbTirages--;
		}
		
		// Mettre les résultats dans l'ordre à l'exception du numéro bonus
		int bonus = resultat[resultat.length - 1];
		resultat[resultat.length - 1] = numeroMax + 1;
		Arrays.sort(resultat);
		resultat[resultat.length - 1] = bonus;
		
		return resultat;
		
	}
	
	static int[] encoderGrille(int nbNumeros, int numeroMax) {
		int[] grille = new int[nbNumeros];
		
		int i = 0;
		int nouveauNombre;
		while (i <= nbNumeros - 1) {
			System.out.printf("Numéro %d ? ", i + 1);
			nouveauNombre = io.Console.lireInt();
			if (util.TableauEntiers.contient(grille, nouveauNombre)) {
				System.out.println("Vous avez déjà encodé ce nombre, veuillez réessayer...");
			} else if (nouveauNombre > numeroMax) {
				System.out.printf("Le nombre que vous avez entré est trop grand, le nombre maximum est %d\n", numeroMax);
			} else {
				grille[i] = nouveauNombre;
				i++;
			}
		}
		
		return grille;
	}
	
	static int[] compterNumerosGagnants(int[] tirage, int[] grille) {
		int compteur = 0;
		for (int i = 0; i < grille.length - 1; i++) {
			if (util.TableauEntiers.contient(tirage, grille[i])) {
				compteur++;
			}
		}
		
		int bonus = 0;
		if (util.TableauEntiers.contient(tirage, grille[grille.length - 1])) {
			bonus = 1;
		}
		
		int[] resultat = {compteur, bonus};
		
		return resultat;
	}
	
	static int determinerRang(int[] numerosGagnants) {
		int rang = (7 - numerosGagnants[0]) * 2 - 1 - numerosGagnants[1];
		return rang;
	}
	
	static double obtenirGain(int rang) {
		double montant = 0.0;
		switch (rang) {
		case 1: montant = 500000.00; break;
		case 2: montant =  75000.00; break;
		case 3: montant =   1500.00; break;
		case 4: montant =    250.00; break;
		case 5: montant =     30.00; break;
		case 6: montant =     10.00; break;
		case 7: montant =      5.00; break;
		case 8: montant =      3.00; break;
		}
		
		return montant;
	}
}
