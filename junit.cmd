@echo off

setlocal EnableDelayedExpansion

set jarFilePath=lib\junit-platform-console-standalone-1.9.1.jar

if not exist lib\ (
    echo The folder "lib" is missing or misspelled.
) else if not exist !jarFilePath! (
    echo The file "!jarFilePath!" is missing.
) else if not exist %1 (
	echo The file "%1" does not exist.
) else if not "%~x1" == ".java" (
	echo The file "%1" is not a Java file.
) else (
	set classFilePath=%1
	set classFilePath=!classFilePath:~0,-5!
	set classAccess=!classFilePath:\=.!
	set classFilePath=!classFilePath!.class
	
	if exist !classFilePath! (
		del !classFilePath!
	)
	
	javac -cp !jarFilePath!;. %1
	
	if exist !classFilePath! (
		java -jar !jarFilePath! -cp . --select-class !classAccess! --disable-banner --disable-ansi-colors
	)
)

endlocal