package labo3;

public class Emprunt {
    public static double calculerTauxMensuel(double tauxAnnuelPourcentage) {
        double tauxAnnuel = tauxAnnuelPourcentage / 100;
        double tauxMensuel = Math.pow((1.0 + tauxAnnuel), 1.0/12) - 1.0;
        return tauxMensuel;
    }

    public static double calculerMensualite(double capital, int periodeAnnees, double tauxAnnuelPourcentage) {
        double tauxMensuel = calculerTauxMensuel(tauxAnnuelPourcentage);
        int periodeMois = periodeAnnees * 12;
        double mensualite = capital * (tauxMensuel / (1 - Math.pow((1 + tauxMensuel), -periodeMois)));
        return mensualite;
    }
}
