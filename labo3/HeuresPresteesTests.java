package labo3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class HeuresPresteesTests {

	// public static int lireHeures(String hhmm)
	
	@Test
	void testLireHeures() {
		assertEquals( 7, HeuresPrestees.lireHeures("7:35"));
		assertEquals(10, HeuresPrestees.lireHeures("10:05"));
	}

	// public static int lireMinutes(String hhmm)

	@Test
	void testLireMinutes() {
		assertEquals(35, HeuresPrestees.lireMinutes("7:35"));
		assertEquals( 5, HeuresPrestees.lireMinutes("10:05"));
	}
	
	// public static int convertirEnMinutes(String hhmm)

	/*@Test
	void testConvertirEnMinutes() {
		assertEquals(   0, HeuresPrestees.convertirEnMinutes("0:00"));
		assertEquals( 455, HeuresPrestees.convertirEnMinutes("7:35"));
		assertEquals( 605, HeuresPrestees.convertirEnMinutes("10:05"));
		assertEquals(1439, HeuresPrestees.convertirEnMinutes("23:59"));
	}*/
	
}
