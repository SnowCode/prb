package labo3;
import io.Console;

public class NormeVecteur {
    public static void main(String[] args) {
        System.out.print("Composante x du vecteur ? ");
        double x = Console.lireDouble();

        System.out.print("Composante y du vecteur ? ");
        double y = Console.lireDouble();

        double norme = calculerNormeVecteur(x, y);
        System.out.printf("La norme du vecteur (%.1f, %.1f) vaut %.1f\n", x, y, norme);
    }

    public static double calculerNormeVecteur(double a, double b) {
        return Math.hypot(a, b);
    }
}
