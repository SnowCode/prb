package labo3;
import io.Console;

public class HeuresPrestees {
    public static void main (String[] args) {
        double tarifHoraire = 18.75;
        double tarifMinutes = tarifHoraire / 60;

        String[] jours = { "lundi", "mardi", "mercredi", "jeudi", "vendredi" };
        int minutesTotales = 0;
        for (String jour: jours) {
            System.out.printf("Prestations du %s ? ", jour);
            String hhmm = Console.lireString();
            minutesTotales += convertirEnMinutes(hhmm);
        }

        int heures = minutesTotales / 60;
        int minutes = minutesTotales % 60;
        System.out.printf("Durée hebdomadaire : %d:%d\n", heures, minutes);

        double montantTotal = minutesTotales * tarifMinutes;
        System.out.printf("Montant à facturer : %.2f EUR\n", montantTotal);
    }

    public static int lireHeures(String hhmm) {
        return Integer.parseInt(hhmm.substring(0, hhmm.length() - 3));
    }

    public static int lireMinutes(String hhmm) {
        return Integer.parseInt(hhmm.substring(hhmm.length() - 2, hhmm.length()));
    }

    public static int convertirEnMinutes(String hhmm) {
        int heures = lireHeures(hhmm);
        int minutes = lireMinutes(hhmm);
        return heures * 60 + minutes;
    }
}

