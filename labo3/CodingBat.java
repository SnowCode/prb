package labo3;

public class CodingBat {
    public static void main(String[] args) {
        System.out.println(helloName("Roger"));
        System.out.println(makeAbba("A", "B"));
        System.out.println(makeTags("h1", "mon titre"));
        System.out.println(makeOutWord("<<>>", "test"));
        System.out.println(extraEnd("hello"));
        System.out.println(firstTwo("hello"));
        System.out.println(firstHalf("hello"));
        System.out.println(withoutEnd("hello"));
    }

    public static String helloName(String name) {
        return "Hello " + name + "!";
    }

    public static String makeAbba(String a, String b) {
        return a + b + b + a;
    }

    public static String makeTags(String tag, String word) {
        return "<" + tag + ">" + word + "</" + tag + ">";
    }

    public static String makeOutWord(String out, String word) {
        String premier = out.substring(0, 2);
        String deuxieme = out.substring(2, 4);
        return premier + word + deuxieme;
    }

    public static String extraEnd(String str) {
        String end = str.substring(str.length() - 2, str.length());
        return end + end + end;
    }

    public static String firstTwo(String str) {
        if (str.length() < 2) {
            return str;
        } else {
            return str.substring(0, 2);
        }
    }

    public static String firstHalf(String str) {
        return str.substring(0, str.length() / 2);
    }

    public static String withoutEnd(String str) {
        return str.substring(1, str.length() - 1);
    }
}

