package util;

public class TableauChaines {
	public static void permuter(String[] tableau, int i, int j) {
		String valJ = tableau[j];
		tableau[i] = valJ;
		tableau[j] = tableau[i];
	}
	
	public static void melanger(String[] tableau) {
		for (int i = 0; i < tableau.length; i++) {
			permuter(tableau, i, io.Aleatoire.aleatoire(0, tableau.length - 1));
		}
	}
	
	public static String toString(String[] tableau, int nbElements) {
		String resultat = "";
		for (int i = 0; i < nbElements; i++) {
			resultat += tableau[i];
			if (i != nbElements - 1) {
				resultat += ", ";
			}
		}
		return resultat;
	}
}
