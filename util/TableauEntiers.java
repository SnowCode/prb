package util;

public class TableauEntiers {
	public static boolean contient(int[] tableau, int a) {
		boolean resultat = false;
		for (int element: tableau) {
			if (element == a) {
				resultat = true;
				break;
			}
		}
		
		return resultat;
	}
	
	public static void permuter(int[] tableau, int i, int j) {
		int valJ = tableau[j];
		tableau[i] = valJ;
		tableau[j] = tableau[i];
	}
	
	public static String toString(int[] tableau) {
		String resultat = "";
		for (int element: tableau) {
			resultat += element + ", ";
		}
		resultat = resultat.replaceAll(", $", "");
		return resultat;
	}
}

