package labo1;
import io.Console;

public class ConversionsTemperature {
    public static void main(String[] args) {
        System.out.print("C° > ");
        double tCelsius = Console.lireDouble();
        System.out.print("K° > ");
        double tKelvin = Console.lireDouble();
        System.out.print("F° > ");
        double tFarhenheit = Console.lireDouble();

        if (tCelsius == 666.66 && tKelvin != 666.66) {
            tCelsius = KtC(tKelvin);
        } else if (tCelsius == 666.66 && tFarhenheit != 666.66) {
            tCelsius = FtC(tFarhenheit);
        }

        tKelvin = CtK(tCelsius);
        tFarhenheit = CtF(tCelsius);

        // tKelvin = tCelsius + 273.15;
        // tFarhenheit = 9/5 * tCelsius + 32;

        System.out.print(tCelsius);
        System.out.println("°C");
        System.out.print(tKelvin);
        System.out.println("K");
        System.out.print(tFarhenheit);
        System.out.println("°F");
    }

    public static double CtK(double tCelsius) {
        double tKelvin = tCelsius + 273.15;
        return tKelvin;
    }

    public static double CtF(double tCelsius) {
        double tFarhenheit = 9/5 * tCelsius + 32;
        return tFarhenheit;
    }

    public static double KtC(double tKelvin) {
        double tCelsius = tKelvin - 273.15;
        return tCelsius;
    }

    public static double FtC(double tFarhenheit) {
        double tCelsius = (tFarhenheit - 32) / (9/5);
        return tCelsius;
    }
}
