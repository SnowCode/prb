package labo1;

public class Inconnu {
    public static void main(String[] args) {
        int nombre = 7805;
        int somme = 0;
        int chiffre;

        chiffre = nombre % 10;      // 5
        somme = somme + chiffre;    // 5
        nombre = nombre / 10;       // 780

        chiffre = nombre % 10;      // 0
        somme = somme + chiffre;    // 5
        nombre = nombre / 10;       // 78

        chiffre = nombre % 10;      // 8
        somme = somme + chiffre;    // 13
        nombre = nombre / 10;       // 7

        chiffre = nombre % 10;      // 7
        somme = somme + chiffre;    // 20
        nombre = nombre / 10;       // 1

        System.out.println(somme);  // 20
    }
}
