package labo5;
import io.Console;

public class FizzBuzz {
    public static void main(String[] args) {
        /* 1. Déclaration des variables */
        int dernierNombre;

        /* 2. Acquisition */
        System.out.print("Dernier nombre ? ");
        dernierNombre = Console.lireInt();

        /* 3. Traitement et affichage */
        for (int i=1; i <= dernierNombre; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.printf("fizzbuzz");
            } else if (i % 3 == 0) {
                System.out.print("fizz");
            } else if (i % 5 == 0) {
                System.out.print("buzz");
            } else {
                System.out.printf("%d", i);
            }

            if (i == dernierNombre) {
                System.out.print("\n");
            } else {
                System.out.print(", ");
            }
        }
    }
}
