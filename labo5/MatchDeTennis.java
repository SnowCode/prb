package labo5;
import io.Console;

public class MatchDeTennis {
    public static void main(String[] args) {
        /* 1. Déclaration des variables */
        String joueur1;
        String joueur2;
        int pointsJoueur1 = 0;
        int pointsJoueur2 = 0;

        /* 2. Acquisition des variables */
        System.out.print("Joueur 1 ? ");
        joueur1 = Console.lireString();
        System.out.print("Joueur 2 ? ");
        joueur2 = Console.lireString();

        /* 3. Traitement et affichage */
        System.out.print("\n");
        while ((pointsJoueur1 < 4 && pointsJoueur2 < 4) || Math.abs(pointsJoueur1 - pointsJoueur2) != 2) {
            System.out.printf("%s\t\t\t%10s\n", joueur1, getScore(pointsJoueur1));
            System.out.printf("%s\t\t\t%10s\n\n", joueur2, getScore(pointsJoueur2));
            System.out.print("Quel joueur gagne le point (1 ou 2) ? ");
            switch (Console.lireInt()) {
                case 1:
                    pointsJoueur1++;
                    break;
                case 2:
                    pointsJoueur2++;
                    break;
            }

            if (pointsJoueur1 == 4 && pointsJoueur2 == 4) {
                pointsJoueur1 = 3;
                pointsJoueur2 = 3;
            }
        }

        if (pointsJoueur1 > pointsJoueur2) {
            System.out.printf("\n%s remporte le jeu !\n", joueur1);
        } else {
            System.out.printf("\n%s remporte le jeu !\n", joueur2);
        }
    }

    static String getScore(int points) {
        String returnValue = " ?";
        switch (points) {
            case 0:
                returnValue = " 0";
                break;
            case 1:
                returnValue = "15";
                break;
            case 2:
                returnValue = "30";
                break;
            case 3:
                returnValue = "40";
                break;
            case 4:
                returnValue = " A";
        }


        return returnValue;
    }
}
