package labo5;
import io.Console;

public class PuissancesDe2 {
    public static void main(String[] args) {
        /* 1. Déclaration des variables */
        // Variables d'acquisitions
        long expMaximum;

        // Variables de traitement
        long exposant = 1;
        long puissance = 1;

        /* 2. Acquisition des variables */
        System.out.print("Exposant maximum ? ");
        expMaximum = Console.lireLong();

        /* 3-4. Traitement et affichage */
        System.out.printf("Exp.\t%20s\n", "Puissance");
        System.out.printf("%d\t%20d\n", 0, 1);
        while (exposant <= expMaximum && exposant != 63) {
            puissance *= 2;
            System.out.printf("%d\t%20d\n", exposant, puissance);
            exposant += 1;
        }
    }
}
