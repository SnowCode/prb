package labo5;
import io.Console;
import io.Aleatoire;

public class De {
    public static void main(String[] args) {
        /* 1. Déclaration des variables */
        // Variables d'acquisition
        int objectif;

        // Variables de traitement
        int lances = 0;
        int dernier = 10;

        /* 2. Acquisition */
        System.out.print("Nombre de 1 à 6 ? ");
        objectif = Console.lireInt();

        /* 3. Traitement */
        do {
            dernier = Aleatoire.aleatoire(1, 6);
            lances += 1;
            System.out.println(dernier);
        } while (dernier != objectif);

        /* 4. Affichage des résultats */
        System.out.printf("Nombre de lancés = %d\n", lances);
    }
}
